package com.zhu.redis.service;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.zhu.redis.BaseTest;
import com.zhu.redis.entity.Area;

public class AreaServiceTest extends BaseTest {
	
	@Autowired
	private AreaService areaService;
	
	@Test
	public void test() {
		List<Area> areaList = areaService.getAreaList();
		for(Area area : areaList) {
			System.out.println(area.getAreaName());
		}
		System.out.println(areaList.size());
	}

}
